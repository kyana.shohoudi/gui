import matplotlib.pyplot as plt
print ("Enter the value of x1: ")
x1 = int(input())
print("Enter yhe value of y1: ")
x2 = int(input())
print("Enter the value of x2: ")
y1 = int(input())
print("Enter the value of y2: ")
y2 = int(input())

dx = x2 - x1
dy = y2 - y1

if abs(dx) > abs(dy) : 
    steps = abs(dx)
else:
    steps = abs(dy)

xincrement = dx/steps
yincrement = dy/steps

i = 0

xcoordinates = []
ycoordinates = []

while i < steps:
    i+=1
    x1 = x1 + xincrement
    
    y1 = y1 + yincrement
    print("x1: ",x1, "y1: ",y1)
    xcoordinates.append(x1)
    ycoordinates.append(y1)
plt.plot(xcoordinates, ycoordinates,color='blue',linestyle='None',marker='+')

#nameing the axis
plt.xlabel("X")
plt.ylabel("Y")

#Graph tittle
plt.title("DDA")
#show the plot
plt.show()
